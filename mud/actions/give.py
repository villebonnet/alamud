# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2, Action3
from mud.events import GiveEvent, GiveToEvent

class GiveAction(Action2):
    EVENT = GiveEvent
    RESOLVE_OBJECT = "resolve_for_use"
    ACTION = "give"

class GiveToAction(Action3):
    EVENT = GiveToEvent
    RESOLVE_OBJECT = "resolve_for_use"
    RESOLVE_OBJECT2 = "resolve_for_operate"
    ACTION = "give-to"
